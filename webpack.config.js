const isProd             = (process.env.NODE_ENV === 'production');
const path               = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const ExtractTextPlugin  = require('extract-text-webpack-plugin');
const DashboardPlugin    = require('webpack-dashboard/plugin');
const webpack            = require('webpack');


const setConfig = () => {
    let config = {

        target: 'node',

        // Main JS File
        entry: {
            app: [
                path.resolve(`./sources/js/main.js`),
                path.resolve(`./sources/scss/styles.scss`),
            ]
        },


        // Output file after compilation
        output: {
            path: path.resolve(`./www/assets`)
        },


        // Set aliases for sources folders
        resolve: {
            alias: {
                '@js'     : path.resolve(`./sources/js/`),
                '@css'    : path.resolve(`./sources/scss/`),
                '@images' : path.resolve(`./sources/images/`),
                '@fonts'  : path.resolve(`./sources/fonts/`),
            }
        },


        // Default settings for each environment
        mode: process.env.NODE_ENV,


        // Set sourcemaps if environment is not prod
        devtool: isProd ? false : 'cheap-module-source-map',

        // Webpack is watching sources if environment is not prod
        watch: !isProd,

        // Rules for each type of files
        module: {
            rules:[
                {
                    test: /\.js$/, // Javascripts
                    exclude: /(node_modules)/,
                    include: [path.resolve('node_modules/framework7')],
                    use: [
                        { loader : 'babel-loader',  query: { presets: ['env']} }
                    ]
                },
                {
                    test: /(\.scss|\.less|\.css)$/, // Sass
                    use: ExtractTextPlugin.extract({
                        fallback: 'style-loader',
                        use: [
                            { loader: 'css-loader', options: { importLoaders: 1, minimize: isProd } },
                            { loader: 'postcss-loader', options: {
                                plugins: [
                                    require('autoprefixer')({
                                        browsers: ['last 5 versions']
                                    }),
                                ]
                            }},
                            { loader: 'sass-loader'}
                        ]
                    })
                },
                {
                    test: /\.(svg|woff2?|ttf|otf|eot)(\?.*)?$/, // Fonts
                    use: [
                        { loader: 'file-loader' },
                    ]
                },
                /*{
                    test: /\.(jpe?g|png|gif|bmp|svg)$/, // images
                    use: [
                        { loader: 'url-loader', options: { limit: 10 * 1024 }},
                        { loader: 'img-loader', options: {
                            plugins: [
                                require('imagemin-gifsicle')({
                                    interlaced: false
                                }),
                                require('imagemin-mozjpeg')({
                                    progressive: true,
                                    arithmetic: false
                                }),
                                require('imagemin-pngquant')({
                                    floyd: 0.5,
                                    speed: 2
                                }),
                                require('imagemin-svgo')({
                                    plugins: [
                                        { removeTitle: true },
                                        { convertPathData: false }
                                    ]
                                })
                            ]}
                        }
                    ]
                },*/
            ]
        },


        // Plugins
        plugins: [
            new CleanWebpackPlugin([path.resolve(`./www/assets`)]),
            new ExtractTextPlugin('[name].css'),
            new DashboardPlugin(),
            new webpack.DefinePlugin({
                'process.env.FLUENTFFMPEG_COV': false
            }),
        ]
    };

    return config;
};


module.exports = [
    setConfig()
];
