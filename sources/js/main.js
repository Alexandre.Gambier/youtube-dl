import Framework7 from 'framework7/js/framework7.bundle.min.js';
// Import F7 Style
import 'framework7/css/framework7.bundle.min.css';
import async from 'async';
const $ = window.$ = require('jquery');
const wv = document.getElementById('webview');

var number = 0;

const fs = require('fs');
const ytdl = require('ytdl-core');

var app = new Framework7({
  // App root element
  root: '#app',
  // App Name
  name: 'Youtube downloader',
  // App id
  id: '',
  // Enable swipe panel
  panel: {
    //swipe: 'left',
  },
  // Add default routes
  routes: [
    {
        name: '',
        path: '/',
        url: '',
        on: {
            pageAfterIn: function test (e, page) {
              // do something after page gets into the view
            },
            pageInit: function (e, page) {

            },
        }
    },
  ],
  // ... other parameters
});

app.views.create('.view-main');

wv.addEventListener('crashed', e => {
    wv.reload();
});

wv.addEventListener('plugin-crashed', e => {
    wv.reload();
});

$('#reload').on('click', function(){
    wv.reload();
});

$('.download').on('click', function(){

    let formData = app.form.convertToData('#my-form');
    let folder = $('#folder');
    let url = wv.getURL();
    if( url == "" || folder.val() == "" ) return app.dialog.alert('Merci de remplir toutes les informations');
    playlist( url, folder.val(), formData.video.length==1)
});


function playlist(url, folder, mode) {
    try{
        var video = ytdl(url);
    } catch(e){
        app.dialog.alert(e);
        $('.download').show();
    }

    video.on('info', function(info) {

        let name = info.title.replace('/', '_');
        let current = number++;

        video.pipe(fs.createWriteStream( folder + '/' + name + '.mp4' ));

        $('#downloads').append(`<li id="${current}"><div class="item-content">
            <div class="item-inner">
                <div class="item-title">${name}
                    <div class="item-footer">
                        <div class="progressbar" id="progress-${current}">
                            <span></span>
                        </div>
                    </div>
                </div>
            </div>
        </div></li>`);

        video.on('progress', function data(chunk, downloaded, total) {
            let percent = (downloaded / total * 100).toFixed(2);
            app.progressbar.set(`#progress-${current}`, percent);
            if(percent == 100){
                if(!mode){
                    $(`#progress-${current}`).toggleClass('progressbar').toggleClass('progressbar-infinite');
                    getMP3(folder+'/'+name, current, name);
                } else {
                    sendNotif(`Téléchargement de ${name} terminé !`)
                    $(`#${current}`).remove();
                }
            }
        });
    });
}

window.removeId = function(datas){
    sendNotif(`Téléchargement de ${datas.name} terminé !`)
    $(`#${datas.id}`).remove();
}
