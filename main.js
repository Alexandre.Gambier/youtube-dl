// Modules to control application life and create native browser window
const {app, BrowserWindow, dialog, systemPreferences} = require('electron');

const ffmpegPath = require('@ffmpeg-installer/ffmpeg').path.replace('app.asar', 'app.asar.unpacked');
const ffmpeg = require('fluent-ffmpeg');
const fs = require('fs');
ffmpeg.setFfmpegPath(ffmpegPath);


// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;

function createWindow () {


  // Create the browser window.
    mainWindow = new BrowserWindow({
        width: 1200,
        height: 900,
        webPreferences: {
          nodeIntegration: true,
          webviewTag: true
        }
    });

  // and load the index.html of the app.
    mainWindow.loadFile('index.html');

    // Open the DevTools.
    // mainWindow.webContents.openDevTools()

    // Emitted when the window is closed.
    mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
        mainWindow = null;
    });


    mainWindow.webContents.on('ipc-message', (e, channel, message) => {
        switch (channel) {
            case 'getFolder':
                let folder = dialog.showOpenDialog({ properties: ['openDirectory'] });
                if(folder != ''){
                    mainWindow.webContents.send('getFolder', folder);
                }
                break;
            case 'getMP3':
                ffmpeg(message.path + '.mp4').save(message.path + '.mp3').on('end', function() {
                    fs.unlink(message.path + '.mp4', (err) => {
                        if (err) return dialog.showErrorBox('Youtube downloader', err);
                        mainWindow.webContents.send('convertionEnd', {id:message.id, name:message.name});
                    });
                });
                break;
            case 'ready':
                mainWindow.webContents.send('darkMode', systemPreferences.isDarkMode());
                break;
            default:

        }
    });

    systemPreferences.subscribeNotification(
        'AppleInterfaceThemeChangedNotification',
        function theThemeHasChanged () {
            mainWindow.webContents.send('darkMode', systemPreferences.isDarkMode());
    });

}



// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') app.quit();
});

app.on('activate', function () {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) createWindow();

});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
